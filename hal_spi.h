#ifndef __hal_spi_h__
#define __hal_spi_h__

#ifdef __cplusplus
  extern "C" {
#endif

/* spi module (stm32f0xx_hal_spi.h) -------------------------------------------------- */
    /* Defines */
#define _hal_spi_mode_master                SPI_MODE_MASTER
#define _hal_spi_mode_slave                 SPI_MODE_SALVE

#define _hal_spi_direction_2lines           SPI_DIRECTION_2LINES
#define _hal_spi_direction_1line            SPI_DIRECTION_1LINE

#define _hal_spi_datasize_8bit              SPI_DATASIZE_8BIT

#define _hal_spi_polarity_low               SPI_POLARITY_LOW
#define _hal_spi_polarity_high              SPI_POLARITY_HIGH

#define _hal_spi_phase_1edge                SPI_PHASE_1EDGE
#define _hal_spi_phase_2edge                SPI_PHASE_2EDGE

#define _hal_spi_nss_soft                   SPI_NSS_SOFT

#define _hal_spi_baudrate_prescaler_64      SPI_BAUDRATEPRESCALER_64
#define _hal_spi_baudrate_prescaler_128     SPI_BAUDRATEPRESCALER_128
#define _hal_spi_baudrate_prescaler_256     SPI_BAUDRATEPRESCALER_256

#define _hal_spi_crccalculation_disable     SPI_CRCCALCULATION_DISABLE

#define _hal_spi_firstbit_msb               SPI_FIRSTBIT_MSB
#define _hal_spi_firstbit_lsb               SPI_FIRSTBIT_LSB

#define _hal_spi_timode_disable             SPI_TIMODE_DISABLE
#define _hal_spi_timode_enable              SPI_TIMODE_ENABLE
    /* Macros */
    /* Typedefs */
#define hal_spi_handle_t                    SPI_HandleTypeDef
    /* Structure elements */
#define mode                                Mode
#define direction                           Direction
#define data_size                           DataSize
#define clk_polarity                        CLKPolarity
#define clk_phase                           CLKPhase
#define nss                                 NSS
#define baudrate_prescaler                  BaudRatePrescaler
#define first_bit                           FirstBit
#define ti_mode                             TIMode
#define crc_calculation                     CRCCalculation

    /* Functions */
#define hal_spi_init                        HAL_SPI_Init
#define hal_spi_mspInit                     HAL_SPI_MspInit
#define hal_spi_transmit                    HAL_SPI_Transmit

#ifdef __cplusplus
}
#endif

#endif
