#ifndef __hal_pcd_h__
#define __hal_pcd_h__

#ifdef __cplusplus
  extern "C" {
#endif

/* timers module (stm32f0xx_hal_tim.h) -------------------------------------------------- */
    /* Defines */
#define _hal_pcd_phy_ulpi                   PCD_PHY_ULPI    
#define _hal_pcd_phy_embedded               PCD_PHY_EMBEDDED
#define _hal_pcd_phy_utmi                   PCD_PHY_UTMI

#define _hal_pcd_speed_full                 PCD_SPEED_FULL
#define _hal_pcd_speed_high                 PCD_SPEED_HIGH

#define _hal_pcd_sng_buf                    PCD_SNG_BUF

#define _hal_pcd_l0_active                  PCD_LPM_L0_ACTIVE
#define _hal_pcd_l1_active                  PCD_LPM_L1_ACTIVE
    /* Macros */
#define __hal_pcd_gate_phyclock             __HAL_PCD_GATE_PHYCLOCK
#define __hal_pcd_ungate_phyclock           __HAL_PCD_UNGATE_PHYCLOCK
    /* Typedefs */
#define hal_pcd_handle_t                    PCD_HandleTypeDef
#define hal_pcd_lpm_t                       PCD_LPM_MsgTypeDef
    /* Structure elements */
    /* Functions */
#define hal_pcd_init                        HAL_PCD_Init
#define hal_pcd_deInit                      HAL_PCD_DeInit
#define hal_pcd_mspInit                     HAL_PCD_MspInit
#define hal_pcd_irqHandler                  HAL_PCD_IRQHandler
#define hal_pcd_start                       HAL_PCD_Start
#define hal_pcd_stop                        HAL_PCD_Stop
#define hal_pcd_epOpen                      HAL_PCD_EP_Open
#define hal_pcd_epClose                     HAL_PCD_EP_Close
#define hal_pcd_epFlush                     HAL_PCD_EP_Flush
#define hal_pcd_epSetStall                  HAL_PCD_EP_SetStall
#define hal_pcd_epClrStall                  HAL_PCD_EP_ClrStall
#define hal_pcd_setAddress                  HAL_PCD_SetAddress
#define hal_pcd_epTransmit                  HAL_PCD_EP_Transmit
#define hal_pcd_epReceive                   HAL_PCD_EP_Receive
#define hal_pcd_epGetRxCount                HAL_PCD_EP_GetRxCount
#define hal_pcd_callback_setupStage         HAL_PCD_SetupStageCallback
#define hal_pcd_callback_dataOutStage       HAL_PCD_DataOutStageCallback
#define hal_pcd_callback_dataInStage        HAL_PCD_DataInStageCallback
#define hal_pcd_callback_sof                HAL_PCD_SOFCallback
#define hal_pcd_callback_reset              HAL_PCD_ResetCallback
#define hal_pcd_callback_suspend            HAL_PCD_SuspendCallback
#define hal_pcd_callback_resume             HAL_PCD_ResumeCallback
#define hal_pcd_callback_isoOutIncomplete   HAL_PCD_ISOOUTIncompleteCallback
#define hal_pcd_callback_isoInIncomplete    HAL_PCD_ISOINIncompleteCallback
#define hal_pcd_callback_connect            HAL_PCD_ConnectCallback
#define hal_pcd_callback_disconnect         HAL_PCD_DisconnectCallback

#define hal_pcdex_pmaConfig                 HAL_PCDEx_PMAConfig
#define hal_pcdex_setTxFifo                 HAL_PCDEx_SetTxFiFo
#define hal_pcdex_setRxFifo                 HAL_PCDEx_SetRxFiFo
#define hal_pcdex_callback_lpm              HAL_PCDEx_LPM_Callback

#ifdef __cplusplus
}
#endif

#endif
