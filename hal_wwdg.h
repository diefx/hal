#ifndef __hal_wwdg_h__
#define __hal_wwdg_h__

#ifdef __cplusplus
  extern "C" {
#endif


/* wwdg module (stm32f0xx_hal_wwdg.h) -------------------------------------------------- */
    /* Defines */
#define _hal_wwdg_prescaler_8               WWDG_PRESCALER_8

#define _hal_wwdg_ewi_disable               WWDG_EWI_DISABLE
#define _hal_wwdg_ewi_enable                WWDG_EWI_ENABLE
    /* Macros */
    /* Typedefs */
#define hal_wwdg_handle_t                   WWDG_HandleTypeDef
    /* Structure elements */
#define instance                            Instance
#define init                                Init

#define preescaler                          Preescaler
#define window                              Window
#define counter                             Counter
#define ewi_mode                            EWIMode   
    /* Functions */
#define hal_wwdg_init                       HAL_WWDG_Init
#define hal_wwdg_mspInit                    HAL_WWDG_MspInit
#define hal_wwdg_refresh                    HAL_WWDG_Refresh
#define hal_wwdg_irqHandler                 HAL_WWDG_IRQHandler
#define hal_wwdg_callback_earlyWakeup       HAL_WWDG_EarlyWakeupCallback

#ifdef __cplusplus
}
#endif

#endif
