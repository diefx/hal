#ifndef __hal_flash_h__
#define __hal_flash_h__

#ifdef __cplusplus
  extern "C" {
#endif

/* flash module (stm32f0xx_hal_flash.h) -------------------------------------------------------- */
    /* Defines */
#define _hal_flash_latency_0                FLASH_LATENCY_0
#define _hal_flash_latency_1                FLASH_LATENCY_1
#define _hal_flash_latency_3                FLASH_LATENCY_3
#define _hal_flash_latency_4                FLASH_LATENCY_4
#define _hal_flash_latency_7                FLASH_LATENCY_7
    /* Macros */
    /* Typedefs */
#define hal_flash_erase_init_t              FLASH_EraseInitTypeDef
    /* Structure elements */
#define type_erase                          TypeErase
#define page_address                        PageAdress
#define nb_pages                            NbPages
    /* Functions */
#define hal_flash_unLock                    HAL_FLASH_Unlock
#define hal_flash_lock                      HAL_FLASH_Lock
#define hal_flash_erase                     HAL_FLASHEx_Erase
#define hal_flash_program                   HAL_FLASH_Program

#ifdef __cplusplus
}
#endif

#endif
