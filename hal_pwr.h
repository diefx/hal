#ifndef __hal_pwr_h__
#define __hal_pwr_h__

#ifdef __cplusplus
  extern "C" {
#endif

/* pwr module (stm32f0xx_hal_pwr.h) --------------------------------------------------------- */
    /* Defines */
#define _hal_pwr_regulator_voltage_scale1  PWR_REGULATOR_VOLTAGE_SCALE1
#define _hal_pwr_regulator_voltage_scale2  PWR_REGULATOR_VOLTAGE_SCALE2
    /* Macros */
#define __hal_pwr_voltagescaling_config    __HAL_PWR_VOLTAGESCALING_CONFIG
    /* Typedefs */
    /* Structure elements */
    /* Functions */
#define hal_pwr_enableBkupAcess             HAL_PWR_EnableBkUpAccess

#ifdef __cplusplus
}
#endif

#endif
