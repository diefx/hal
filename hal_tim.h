#ifndef __hal_tim_h__
#define __hal_tim_h__

#ifdef __cplusplus
  extern "C" {
#endif

/* timers module (stm32f0xx_hal_tim.h) -------------------------------------------------- */
    /* Defines */
#define _hal_tim_counter_mode_up            TIM_COUNTERMODE_UP

#define _hal_tim_clock_division_div_1       TIM_CLOCKDIVISION_DIV1

#define _hal_tim_autoreload_preload_disable TIM_AUTORELOAD_PRELOAD_DISABLE

#define _hal_tim_oc_mode_pwm_1              TIM_OCMODE_PWM1

#define _hal_tim_oc_polarity_high           TIM_OCPOLARITY_HIGH

#define _hal_tim_oc_fast_disable            TIM_OCFAST_DISABLE

#define _hal_tim_channel_1                  TIM_CHANNEL_1
#define _hal_tim_channel_2                  TIM_CHANNEL_2
#define _hal_tim_channel_3                  TIM_CHANNEL_3
    /* Macros */
    /* Typedefs */
#define hal_tim_handle_t                    TIM_HandleTypeDef
#define hal_tim_oc_t                        TIM_OC_InitTypeDef
    /* Structure elements */
#define instance                            Instance
#define init                                Init
#define prescaler                           Prescaler
#define counter_mode                        CounterMode
#define period                              Period
#define clock_division                      ClockDivision
#define repetition_counter                  RepetitionCounter
#define auto_reload_preload                 AutoReloadPreload

#define oc_mode                             OCMode
#define oc_polarity                         OCPolarity
#define oc_fast_mode                        OCFastMode
#define pulse                               Pulse
    /* Functions */
#define hal_tim_base_init                   HAL_TIM_Base_Init
#define hal_tim_base_mspInit                HAL_TIM_Base_MspInit
#define hal_tim_base_start                  HAL_TIM_Base_Start
#define hal_tim_base_stop                   HAL_TIM_Base_Stop
#define hal_tim_base_intStart               HAL_TIM_Base_Start_IT
#define hal_tim_base_intStop                HAL_TIM_Base_Stop_IT

#ifdef __cplusplus
}
#endif

#endif
