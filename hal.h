#ifndef __hal_h__
#define __hal_h__

#ifdef __cplusplus
  extern "C" {
#endif

#include "cmsis.h"
#include "hal_cortex.h"
#include "hal_flash.h"
#include "hal_pwr.h"
#include "hal_rcc.h"
#include "hal_gpio.h"
#include "hal_uart.h"
#include "hal_i2c.h"
#include "hal_rtc.h"
#include "hal_spi.h"
#include "hal_tim.h"
#include "hal_wwdg.h"
#include "hal_pcd.h"

/* wrapper definitions to make sure we accomplish notation standar */
/* --------------------------------------------------------------------------------------------- */
/* <driver> module (stm32f0xx_hal_<driver>.h) -------------------------------------------------- */
    /* Defines */
    /* Macros */
    /* Typedefs */
    /* Structure elements */
    /* Functions */


/* system module (stm32f0xx_hal_system.h) ------------------------------------------------------ */
    /* Defines */
    /* Macros */
#define __sys_assert                        assert_param
    /* Typedefs */
    /* Structure elements */
    /* Functions */
#define hal_system_init                     SystemInit
#define hal_system_coreClockUpdate          SystemCoreClockUpdate


/* hal module (stm32f0xx_hal.h) ---------------------------------------------------------------- */
    /* Defines */
#define _hal_set                            SET
#define _hal_reset                          RESET
    /* Macros */
    /* Typedefs */
    /* Structure elements */
    /* Functions */
#define hal_init                            HAL_Init
#define hal_deInit                          HAL_DeInit
#define hal_mspInit                         HAL_MspInit
#define hal_mspDeInit                       HAL_MspDeInit
#define hal_delay                           HAL_Delay
#define hal_initTick                        HAL_InitTick
#define hal_incTick                         HAL_IncTick
#define hal_getTick                         HAL_GetTick

#ifdef __cplusplus
}
#endif

#endif
