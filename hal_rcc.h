#ifndef __hal_rcc_h__
#define __hal_rcc_h__

#ifdef __cplusplus
  extern "C" {
#endif

/* rcc module (stm32f0xx_hal_rcc.h) ------------------------------------------------------------ */
    /* Macros */
#define __hal_rcc_gpioa_clk_enable          __HAL_RCC_GPIOA_CLK_ENABLE
#define __hal_rcc_gpiob_clk_enable          __HAL_RCC_GPIOB_CLK_ENABLE
#define __hal_rcc_gpioc_clk_enable          __HAL_RCC_GPIOC_CLK_ENABLE
#define __hal_rcc_gpiod_clk_enable          __HAL_RCC_GPIOD_CLK_ENABLE
#define __hal_rcc_gpioe_clk_enable          __HAL_RCC_GPIOE_CLK_ENABLE
#define __hal_rcc_usart1_clk_enable         __HAL_RCC_USART1_CLK_ENABLE
#define __hal_rcc_usart2_clk_enable         __HAL_RCC_USART2_CLK_ENABLE
#define __hal_rcc_i2c1_clk_enable           __HAL_RCC_I2C1_CLK_ENABLE
#define __hal_rcc_rtc_enable                __HAL_RCC_RTC_ENABLE
#define __hal_rcc_spi1_clk_enable           __HAL_RCC_SPI1_CLK_ENABLE
#define __hal_rcc_spi2_clk_enable           __HAL_RCC_SPI2_CLK_ENABLE
#define __hal_rcc_pwr_clk_enable            __HAL_RCC_PWR_CLK_ENABLE
#define __hal_rcc_wwdg_clk_enable           __HAL_RCC_WWDG_CLK_ENABLE
#define __hal_rcc_tim1_clk_enable           __HAL_RCC_TIM1_CLK_ENABLE
#define __hal_rcc_tim2_clk_enable           __HAL_RCC_TIM2_CLK_ENABLE
#define __hal_rcc_tim3_clk_enable           __HAL_RCC_TIM3_CLK_ENABLE
#define __hal_rcc_tim4_clk_enable           __HAL_RCC_TIM4_CLK_ENABLE
#define __hal_rcc_tim5_clk_enable           __HAL_RCC_TIM5_CLK_ENABLE
#define __hal_rcc_tim8_clk_enable           __HAL_RCC_TIM8_CLK_ENABLE
#define __hal_rcc_tim9_clk_enable           __HAL_RCC_TIM9_CLK_ENABLE
#define __hal_rcc_tim10_clk_enable          __HAL_RCC_TIM10_CLK_ENABLE
#define __hal_rcc_tim11_clk_enable          __HAL_RCC_TIM11_CLK_ENABLE
#define __hal_rcc_tim12_clk_enable          __HAL_RCC_TIM12_CLK_ENABLE
#define __hal_rcc_tim13_clk_enable          __HAL_RCC_TIM13_CLK_ENABLE
#define __hal_rcc_tim14_clk_enable          __HAL_RCC_TIM14_CLK_ENABLE
#define __hal_rcc_usb_clk_enable            __HAL_RCC_USB_CLK_ENABLE
#define __hal_rcc_usb_otg_fs_clk_enable     __HAL_RCC_USB_OTG_FS_CLK_ENABLE
#define __hal_rcc_syscfg_clk_enable         __HAL_RCC_SYSCFG_CLK_ENABLE

    /* Typedefs */
#define hal_rcc_clk_init_t                  RCC_ClkInitTypeDef
#define hal_rcc_osc_init_t                  RCC_OscInitTypeDef
#define hal_rcc_pll_init_t                  RCC_PLLInitTypeDef

#define hal_rcc_periph_clk_init_t           RCC_PeriphCLKInitTypeDef
    /* Defines */
#define _hal_rcc_oscillatortype_none        RCC_OSCILLATORTYPE_NONE
#define _hal_rcc_oscillatortype_hse         RCC_OSCILLATORTYPE_HSE
#define _hal_rcc_oscillatortype_hsi         RCC_OSCILLATORTYPE_HSI
#define _hal_rcc_oscillatortype_lse         RCC_OSCILLATORTYPE_LSE
#define _hal_rcc_oscillatortype_lsi         RCC_OSCILLATORTYPE_LSI
#define _hal_rcc_oscillatortype_hsi14       RCC_OSCILLATORTYPE_HSI14
#define _hal_rcc_oscillatortype_hsi48       RCC_OSCILLATORTYPE_HSI48

#define _hal_rcc_hse_off                    RCC_HSE_OFF
#define _hal_rcc_hse_on                     RCC_HSE_ON
#define _hal_rcc_hse_bypass                 RCC_HSE_BYPASS

#define _hal_rcc_lse_off                    RCC_LSE_OFF
#define _hal_rcc_lse_on                     RCC_LSE_ON
#define _hal_rcc_lse_bypass                 RCC_LSE_BYPASS

#define _hal_rcc_lsi_off                    RCC_LSI_OFF
#define _hal_rcc_lsi_on                     RCC_LSI_ON

#define _hal_rcc_hsi48_off                  RCC_HSI48_OFF
#define _hal_rcc_hsi48_on                   RCC_HSI48_ON

#define _hal_rcc_pll_none                   RCC_PLL_NONE
#define _hal_rcc_pll_off                    RCC_PLL_OFF
#define _hal_rcc_pll_on                     RCC_PLL_ON

#define _hal_rcc_clocktype_sysclk           RCC_CLOCKTYPE_SYSCLK
#define _hal_rcc_clocktype_hclk             RCC_CLOCKTYPE_HCLK
#define _hal_rcc_clocktype_pclk1            RCC_CLOCKTYPE_PCLK1
#define _hal_rcc_clocktype_pclk2            RCC_CLOCKTYPE_PCLK2

#define _hal_rcc_sysclksource_hsi           RCC_SYSCLKSOURCE_HSI
#define _hal_rcc_sysclksource_hse           RCC_SYSCLKSOURCE_HSE
#define _hal_rcc_sysclksource_pllclk        RCC_SYSCLKSOURCE_PLLCLK
#define _hal_rcc_sysclksource_hsi48         RCC_SYSCLKSOURCE_HSI48

#define _hal_rcc_sysclk_div1                RCC_SYSCLK_DIV1
#define _hal_rcc_sysclk_div2                RCC_SYSCLK_DIV2

#define _hal_rcc_hclk_div1                  RCC_HCLK_DIV1
#define _hal_rcc_hclk_div2                  RCC_HCLK_DIV2
#define _hal_rcc_hclk_div4                  RCC_HCLK_DIV2
#define _hal_rcc_hclk_div8                  RCC_HCLK_DIV8

#define _hal_rcc_rtcclksource_lse           RCC_RTCCLKSOURCE_LSE
#define _hal_rcc_rtcclksource_lsi           RCC_RTCCLKSOURCE_LSI
#define _hal_rcc_i2c2clksource_pclk1        RCC_I2C2CLKSOURCE_PCLK1
#define _hal_rcc_clk48source_pll            RCC_CLK48SOURCE_PLL
#define _hal_rcc_usbclksource_hsi48         RCC_USBCLKSOURCE_HSI48

#define _hal_rcc_pll_mul2                   RCC_PLL_MUL2
#define _hal_rcc_pll_mul3                   RCC_PLL_MUL3
#define _hal_rcc_pll_mul4                   RCC_PLL_MUL4
#define _hal_rcc_pll_mul12                  RCC_PLL_MUL12

#define _hal_rcc_prediv_div1                RCC_PREDIV_DIV1
#define _hal_rcc_prediv_div2                RCC_PREDIV_DIV2
#define _hal_rcc_prediv_div3                RCC_PREDIV_DIV3
#define _hal_rcc_prediv_div4                RCC_PREDIV_DIV4

#define _hal_rcc_pllp_div2                  RCC_PLLP_DIV2
#define _hal_rcc_pllp_div4                  RCC_PLLP_DIV4

#define _hal_rcc_mco1source_noclock         RCC_MCO1SOURCE_NOCLOCK
#define _hal_rcc_mco1source_lsi             RCC_MCO1SOURCE_LSI
#define _hal_rcc_mco1source_lse             RCC_MCO1SOURCE_LSE
#define _hal_rcc_mco1source_sysclk          RCC_MCO1SOURCE_SYSCLK

#define _hal_rcc_mcodiv_16                  RCC_MCODIV_16
#define _hal_rcc_mcodiv_32                  RCC_MCODIV_32
#define _hal_rcc_mcodiv_64                  RCC_MCODIV_64

#define _hal_rcc_periphclk_i2c2             RCC_PERIPHCLK_I2C2
#define _hal_rcc_periphclk_tim              RCC_PERIPHCLK_TIM
#define _hal_rcc_periphclk_clk48            RCC_PERIPHCLK_CLK48
#define _hal_rcc_periphclk_rtc              RCC_PERIPHCLK_RTC
#define _hal_rcc_periphclk_usb              RCC_PERIPHCLK_USB

#define _hal_rcc_pllsource_hse              RCC_PLLSOURCE_HSE
#define _hal_rcc_pllsource_hsi              RCC_PLLSOURCE_HSI
#define _hal_rcc_pllsource_hsi48            RCC_PLLSOURCE_HSI48

#define _hal_rcc_tim_pres_desactivated      RCC_TIMPRES_DESACTIVATED
    /* Structure elements */
#define oscillator_type                     OscillatorType
#define hse_state                           HSEState
#define lse_state                           LSEState
#define hsi_state                           HSIState
#define hsi14_state                         HSI14State
#define lsi_state                           LSIState
#define hsi48_state                         HSI48State
#define pll                                 PLL

#define pll_state                           PLLState
#define pll_source                          PLLSource
#define pllmul                              PLLMUL
#define prediv                              PREDIV
#define pllm                                PLLM
#define plln                                PLLN
#define pllp                                PLLP
#define pllq                                PLLQ

#define periph_clock_selection              PeriphClockSelection
#define rtc_clock_selection                 RTCClockSelection
#define usb_clock_selection                 UsbClockSelection

#define clock_type                          ClockType
#define sysclk_source                       SYSCLKSource
#define ahbclk_divider                      AHBCLKDivider
#define apb1clk_divider                     APB1CLKDivider
#define apb2clk_divider                     APB2CLKDivider

#define i2c2_clock_selection                I2c2ClockSelection
#define clk48_clock_selection               Clk48ClockSelection
#define tim_pres_selection                  TIMPresSelection
    /* Functions */
#define hal_rcc_oscConfig                   HAL_RCC_OscConfig
#define hal_rcc_clockConfig                 HAL_RCC_ClockConfig
#define hal_rcc_mcoConfig                   HAL_RCC_MCOConfig

#define hal_rccex_periphClkConfig           HAL_RCCEx_PeriphCLKConfig

#ifdef __cplusplus
}
#endif

#endif