#ifndef __hal_i2c_h__
#define __hal_i2c_h__

#ifdef __cplusplus
  extern "C" {
#endif

/* i2c module (stm32f0xx_hal_i2c.h) -------------------------------------------------- */
    /* Defines */
#define _hal_i2c_addressing_mode_7bit       I2C_ADDRESSINGMODE_7BIT
#define _hal_i2c_dual_address_disable       I2C_DUALADDRESS_DISABLE
#define _hal_i2c_duty_cycle_2               I2C_DUTYCYCLE_2
#define _hal_i2c_general_call_disable       I2C_GENERALCALL_DISABLE
#define _hal_i2c_no_stretch_disable         I2C_NOSTRETCH_DISABLE
    /* Macros */
    /* Typedefs */
#define hal_i2c_handle_t                    I2C_HandleTypeDef
    /* Structure elements */
#define init                                Init
#define instance                            Instance
#define timing                              Timing
#define addressing_mode                     AddressingMode
#define dual_address_mode                   DualAddressMode
#define general_call_mode                   GeneralCallMode
#define no_stretch_mode                     NoStretchMode
    /* Functions */
#define hal_i2c_init                        HAL_I2C_Init
#define hal_i2c_masterReceive               HAL_I2C_Master_Receive
#define hal_i2c_mspInit                     HAL_I2C_MspInit

#ifdef __cplusplus
}
#endif

#endif
