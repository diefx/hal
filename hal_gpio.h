#ifndef __hal_gpio_h__
#define __hal_gpio_h__

#ifdef __cplusplus
  extern "C" {
#endif

/* gpio module (stm32fxx_hal_gpio.h) ----------------------------------------------------------- */
    /* Defines */
#define _hal_gpio_pin_0                     GPIO_PIN_0
#define _hal_gpio_pin_1                     GPIO_PIN_1
#define _hal_gpio_pin_2                     GPIO_PIN_2
#define _hal_gpio_pin_3                     GPIO_PIN_3
#define _hal_gpio_pin_4                     GPIO_PIN_4
#define _hal_gpio_pin_5                     GPIO_PIN_5
#define _hal_gpio_pin_6                     GPIO_PIN_6
#define _hal_gpio_pin_7                     GPIO_PIN_7
#define _hal_gpio_pin_8                     GPIO_PIN_8
#define _hal_gpio_pin_9                     GPIO_PIN_9
#define _hal_gpio_pin_10                    GPIO_PIN_10
#define _hal_gpio_pin_11                    GPIO_PIN_11
#define _hal_gpio_pin_12                    GPIO_PIN_12
#define _hal_gpio_pin_13                    GPIO_PIN_13
#define _hal_gpio_pin_14                    GPIO_PIN_14
#define _hal_gpio_pin_15                    GPIO_PIN_15

#define _hal_gpio_mode_input                GPIO_MODE_INPUT
#define _hal_gpio_mode_output_pp            GPIO_MODE_OUTPUT_PP
#define _hal_gpio_mode_output_od            GPIO_MODE_OUTPUT_OD
#define _hal_gpio_mode_af_pp                GPIO_MODE_AF_PP
#define _hal_gpio_mode_af_od                GPIO_MODE_AF_OD
#define _hal_gpio_mode_analog               GPIO_MODE_ANALOG
#define _hal_gpio_mode_it_rising            GPIO_MODE_IT_RISING
#define _hal_gpio_mode_it_falling           GPIO_MODE_IT_FALLING
#define _hal_gpio_mode_it_rising_falling    GPIO_MODE_IT_RISING_FALLING

#define _hal_gpio_speed_freq_very_high      GPIO_SPEED_FREQ_VERY_HIGH
#define _hal_gpio_speed_freq_high           GPIO_SPEED_FREQ_HIGH
#define _hal_gpio_speed_freq_medium         GPIO_SPEED_FREQ_MEDIUM
#define _hal_gpio_speed_freq_low            GPIO_SPEED_FREQ_LOW

#define _hal_gpio_nopull                    GPIO_NOPULL
#define _hal_gpio_pullup                    GPIO_PULLUP
#define _hal_gpio_pulldown                  GPIO_PULLDOWN

#define _hal_gpio_af0_spi1                  GPIO_AF0_SPI1
#define _hal_gpio_af0_spi2                  GPIO_AF0_SPI2
#define _hal_gpio_af1_usart1                GPIO_AF1_USART1
#define _hal_gpio_af1_usart2                GPIO_AF1_USART2
#define _hal_gpio_af1_tim1                  GPIO_AF1_TIM1
#define _hal_gpio_af1_tim2                  GPIO_AF1_TIM2
#define _hal_gpio_af2_tim3                  GPIO_AF2_TIM3
#define _hal_gpio_af2_tim4                  GPIO_AF2_TIM4
#define _hal_gpio_af2_tim5                  GPIO_AF2_TIM5
#define _hal_gpio_af2_usb                   GPIO_AF2_USB
#define _hal_gpio_af3_i2c1                  GPIO_AF9_I2C1
#define _hal_gpio_af3_tim8                  GPIO_AF3_TIM8
#define _hal_gpio_af3_tim9                  GPIO_AF3_TIM9
#define _hal_gpio_af3_tim10                 GPIO_AF3_TIM10
#define _hal_gpio_af3_tim11                 GPIO_AF3_TIM11
#define _hal_gpio_af9_tim12                 GPIO_AF9_TIM12
#define _hal_gpio_af9_tim13                 GPIO_AF9_TIM13
#define _hal_gpio_af9_tim14                 GPIO_AF9_TIM14
#define _hal_gpio_af10_otg_fs               GPIO_AF10_OTG_FS
    /* Macros */
    /* Typedefs */
#define hal_gpio_init_t                     GPIO_InitTypeDef
    /* Structure elements */
#define mode                                Mode
#define pull                                Pull
#define pin                                 Pin
#define freq                                Speed
#define alternate                           Alternate
    /* Functions */
#define hal_gpio_init                       HAL_GPIO_Init
#define hal_gpio_writePin                   HAL_GPIO_WritePin
#define hal_gpio_readPin                    HAL_GPIO_ReadPin
#define hal_gpio_togglePin                  HAL_GPIO_TogglePin
#define hal_gpio_irqHandler                 HAL_GPIO_EXTI_IRQHandler
#define hal_gpio_callback_exti              HAL_GPIO_EXTI_Callback

#ifdef __cplusplus
}
#endif

#endif
