#ifndef __hal_cortex_h__
#define __hal_cortex_h__

#ifdef __cplusplus
  extern "C" {
#endif

/* hal module (stm32f0xx_hal_cortex.h) --------------------------------------------------------- */
    /* Defines */
    /* Macros */
    /* Typedefs */
    /* Structure elements */
    /* Functions */
#define hal_nvic_setPriority                HAL_NVIC_SetPriority
#define hal_nvic_enableIrq                  HAL_NVIC_EnableIRQ

#ifdef __cplusplus
}
#endif

#endif
