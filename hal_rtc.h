#ifndef __hal_rtc_h__
#define __hal_rtc_h__

#ifdef __cplusplus
  extern "C" {
#endif

/* rtc module (stm32f0xx_hal_rtc.h) ---------------------------------------------------- */
    /* Defines */
#define _hal_rtc_hour_format_24             RTC_HOURFORMAT_24
#define _hal_rtc_hour_format_12             RTC_HOURFORMAT_12

#define _hal_rtc_hour_format12_am           RTC_HOURFORMAT12_AM
#define _hal_rtc_hour_format12_pm           RTC_HOURFORMAT12_PM

#define _hal_rtc_format_bin                 RTC_FORMAT_BIN
#define _hal_rtc_format_bcd                 RTC_FORMAT_BCD

#define _hal_rtc_daylightsaving_none        RTC_DAYLIGHTSAVING_NONE

#define _hal_rtc_output_disable             RTC_OUTPUT_DISABLE  
    /* Macros */
    /* Typedefs */
#define hal_rtc_handle_t                    RTC_HandleTypeDef
#define hal_rtc_time_t                      RTC_TimeTypeDef
#define hal_rtc_date_t                      RTC_DateTypeDef
#define hal_rtc_alarm_t                     RTC_AlarmTypeDef
    /* Structure elements */
#define hour_format                         HourFormat
#define asynch_prediv                       AsynchPrediv
#define synch_prediv                        SynchPrediv
#define output                              OutPut

#define hours                               Hours
#define minutes                             Minutes
#define seconds                             Seconds
#define time_format                         TimeFormat
#define subseconds                          SubSeconds
#define day_light_saving                    DayLightSaving

#define weekday                             WeekDay
#define month                               Month
#define date                                Date
#define year                                Year

#define alarm                               Alarm
#define alarm_time                          AlarmTime
    /* Functions */
#define hal_rtc_init                        HAL_RTC_Init
#define hal_rtc_mspInit                     HAL_RTC_MspInit
#define hal_rtc_setTime                     HAL_RTC_SetTime
#define hal_rtc_setDate                     HAL_RTC_SetDate
#define hal_rtc_setAlarm                    HAL_RTC_SetAlarm
#define hal_rtc_getTime                     HAL_RTC_GetTime
#define hal_rtc_getDate                     HAL_RTC_GetDate
#define hal_rtc_getAlarm                    HAL_RTC_GetAlarm
#define hal_rtc_irqHandler                  HAL_RTC_AlarmIRQHandler
#define hal_rtc_callback_alarmEvent         HAL_RTC_AlarmAEventCallback

#ifdef __cplusplus
}
#endif

#endif
