#ifndef __hal_uart_h__
#define __hal_uart_h__

#ifdef __cplusplus
  extern "C" {
#endif

/* uart module (stm32f0xx_hal_uart.h) -------------------------------------------------- */
    /* Defines */
#define _hal_uart_stop_bits_1               UART_STOPBITS_1

#define _hal_uart_parity_none               UART_PARITY_NONE

#define _hal_uart_hw_control_none           UART_HWCONTROL_NONE

#define _hal_uart_mode_rx                   UART_MODE_RX
#define _hal_uart_mode_tx                   UART_MODE_TX
#define _hal_uart_mode_tx_rx                UART_MODE_TX_RX

#define _hal_uart_oversampling_16           UART_OVERSAMPLING_16
#define _hal_uart_oversampling_8            UART_OVERSAMPLING_8

#define _hal_uart_word_length_8b            UART_WORDLENGTH_8B
    /* Macros */ 
    /* Typedefs */
#define hal_uart_handle_t                   UART_HandleTypeDef 
    /* Structure elements */
#define instance                            Instance
#define init                                Init

#define baudrate                            BaudRate
#define word_length                         WordLength
#define stop_bits                           StopBits
#define parity                              Parity
#define hw_flow_ctl                         HwFlowCtl
#define mode                                Mode
#define over_sampling                       OverSampling

    /* Functions */
#define hal_uart_init                       HAL_UART_Init
#define hal_uart_transmit                   HAL_UART_Transmit
#define hal_uart_intTransmit                HAL_UART_Transmit_IT
#define hal_uart_receive                    HAL_UART_Receive
#define hal_uart_intReceive                 HAL_UART_Receive_IT
#define hal_uart_mspInit                    HAL_UART_MspInit
#define hal_uart_irqHandler                 HAL_UART_IRQHandler
#define hal_uart_callback_txComplete        HAL_UART_TxCpltCallback
#define hal_uart_callback_rxComplete        HAL_UART_RxCpltCallback
#define hal_uart_callback_error             HAL_UART_ErrorCallback

#ifdef __cplusplus
}
#endif

#endif
